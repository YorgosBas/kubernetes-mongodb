import express from 'express';
import mongoose from 'mongoose';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(_dirname + '/public'));

app.listen(3000, () => {
    console.log('Server started on port 3000');
});

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    user: process.env.MONGODB_ADMINUSERNAME,
    pass: process.env.MONGODB_ADMINPASSWORD,
    dbName: process.env.MONGODB_DB_NAME
};

mongoose.connect(`mongodb://mongodb-service`, options).catch(err => {
    if (err.message.indexOf('ECONNREFUSED') !== -1) {
        console.error('Error connecting to mongodb: mongodb-service is not running');
        process.exit(1);
    } else {
        throw err;
    }
});

const Schema = mongoose.Schema;
export const QuoteSchema = new Schema({
    quote: { type: String, required: 'Enter quote' }
});

const Quote = mongoose.model('Quote', QuoteSchema);

app.get('/', (req, res) => {
    res.sendFile(_dirname + '/index.html');
});

app.get('/quotes', (req, res) => {
    Quote.find().then(results => {
        res.json(results);
    }).catch(error => {
        res.json({});
    });
});

app.post('/quotes', (req, res) => {
    let newQuote = new Quote(req.body);
    if (req.body.quote === '') {
        return res.json({ error: 'Enter quote' });
    }

    newQuote.save((err, quote) => {
        if (err) {
            res.json({ error: 'error saving quote' });
        }
        res.json(quote);
    });
});

app.delete('/quotes', (req, res) => {
    Quote.deleteOne().then(result => {
        if (result.deleteCount === 0) {
            return res.json({ error: 'no quote to delete' });
        }
        res.json(`Deleted a quote`);
    }).catch(error => console.error(error));
});