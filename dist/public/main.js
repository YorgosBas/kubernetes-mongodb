const quotes = [];
const html_quotes = document.querySelector('#quotes');
const messageDiv = document.querySelector('#message');

fetch('/quotes')
    .then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem');
            return;
        }
        return response.json();
    })

    .then(obj => {
        obj.forEach(data => quotes.push(data.quote));
        html_quotes.innerHTML = quotes.join('<br>');
    })

    .catch(function (error) {
        console.log('Fetching initial quotes failed. Error :-S', error);
    });

const deleteButton = document.querySelector('#delete-button')
deleteButton.addEventListener('click', _ => {
    fetch('/quotes', {
        method: 'delete',
        headers: { 'Content-Type': 'application/json' }
    })
        .then(response => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem');
                return;
            }
            return response.json();
        })

        .then(response => {
           if (response.error) {
               messageDiv.textContent = 'Error: ' + response.error;
           } else {
               quotes.pop();
               html_quotes.innerHTML = quotes.join('<br>');
           }
        })

        .catch(error => console.error(error))
})

const addButton = document.querySelector('#add-button');
addButton.addEventListener('click', _ => {
    let quote = document.querySelector('#quote');
    fetch('/quotes', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ quote: quote.value })
    })
        .then(response => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem');
                return;
            }
            return response.json();
        })

        .then(response => {
            if (response.error) {
                messageDiv.textContent = 'Error: ' + response.error;
            } else {
                quotes.push(quote.value);
                html_quotes.innerHTML = quotes.join('<br>');
            }
        })

        .catch(error => console.error(error))
})