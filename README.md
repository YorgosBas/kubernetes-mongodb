A containerised JavaScript web application using Express and Mongoose with Kubernetes orchestration that
connects the pod of the frontend application with the MongoDB pod for bidirectional communication using ConfigMaps and
Secrets

## Installation
```yaml
https://gitlab.com/YorgosBas/kubernetes-mongodb.git
```
Build the image
```yaml
docker build -t localhost:32000/mongo-app:v3
```
Push the image to the registry
```yaml
docker push localhost:32000/mongo-app:v3
```
Apply configurations
```yaml
microk8s.kubectl apply -f mongo-configmap.yaml
microk8s.kubectl apply -f mongo-secret.yaml
microk8s.kubectl apply -f mongo.yaml
microk8s.kubectl apply -f mongo-express.yaml
```

##### Generating Secrets
```yaml
base64 eccoded: echo -n 'username' | base64
```
Type your secrets inside mongo-secret.yaml
```
data: #base64 eccoded: echo -n 'username' | base64
  mongo-root-username: $MONGODB_ROOT_USERNAME
  mongo-root-password: $MONGODB_ROOT_PASSWORD
```
##### MongoDB service index.js
Type your own service
```
mongoose.connect(`mongodb://mongodb-service`, options)
```